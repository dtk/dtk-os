// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.13
import QtQuick.Window 2.13
import QtGraphicalEffects 1.12

Item {

     width: Screen.width;
    height: Screen.height;

    Image { id: background;

        source: "../../ast/dtkOSRuntimeWidgets/wall_d";
        smooth: true;

        property var r_p: parent.width/parent.height;
        property var r_s: background.sourceSize.width/background.sourceSize.height;

        fillMode: Image.PreserveAspectFit;

        Component.onCompleted: {

            // TODO: Validate many devices once Xcode's display mode is set.
            
            if(r_s > r_p)
                height = parent.height;
            else
                width = parent.width;

            // console.log("Backgd width: " + background.width);
            // console.log("Parent ratio: " + r_p);
            // console.log("Parent w:     " + parent.width);
            // console.log("Parent h:     " + parent.height);
            // console.log("Source ratio: " + r_s);
            // console.log("Source w:     " + background.sourceSize.width);
            // console.log("Source h:     " + background.sourceSize.height);
        }
    }

    Image { id: background_blur_source;

        property var w: 300;
        property var h: 300;

        x: parent.width/2 - w/2;
        y: parent.height/2 - h/2;

        source: background.source;

        width: w;
        height: h;
    }

    // FastBlur {

    //     anchors.fill: parent;

    //     width: 100;
    //     height: 100;

    //     source: background_blur_source;

    //     radius: 32;
    // }
}

//
// dtkOSRuntimeWidgetsSurface.qml ends here

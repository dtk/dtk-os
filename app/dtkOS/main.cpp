// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtDebug>
#include <QtCore>
#include <QtWidgets>

#include <dtkOSRuntimeWidgets>

int main(int argc, char **argv)
{
    QApplication application(argc, argv);
    
    QStackedWidget *screen = new QStackedWidget;

    dtkOSRuntimeWidgetsSurface *boot = new dtkOSRuntimeWidgetsSurface(screen);
    boot->setContents("qrc:dtkOSRuntimeWidgetsSurfaceBoot.qml");

    dtkOSRuntimeWidgetsSurface *login = new dtkOSRuntimeWidgetsSurface(screen);
    login->setContents("qrc:dtkOSRuntimeWidgetsSurfaceLogin.qml");

    dtkOSRuntimeWidgetsSurface *window = new dtkOSRuntimeWidgetsSurface(screen);
    window->setContents("qrc:dtkOSRuntimeWidgetsSurfaceWindow.qml");

    screen->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    screen->addWidget(boot);
    screen->addWidget(login);
    screen->addWidget(window);
    screen->setCurrentWidget(boot);
    screen->setCurrentIndex(0);
    screen->showMaximized();

// /////////////////////////////////////////////////////////////////////////////
// 
// /////////////////////////////////////////////////////////////////////////////
    
    QStateMachine machine;

    QState *state_b = new QState;
    QState *state_l = new QState;
    QState *state_w = new QState;

    state_b->assignProperty(screen, "currentIndex", 0);
    state_l->assignProperty(screen, "currentIndex", 1);
    state_w->assignProperty(screen, "currentIndex", 2);

    state_b->addTransition(boot,  SIGNAL(done()), state_l);
    state_l->addTransition(login, SIGNAL(done()), state_w);

    machine.addState(state_b);
    machine.addState(state_l);
    machine.addState(state_w);
    machine.setInitialState(state_b);
    machine.start();

// /////////////////////////////////////////////////////////////////////////////
// 
// /////////////////////////////////////////////////////////////////////////////
    
    QTimer::singleShot(2000, boot, SIGNAL(done()));

// /////////////////////////////////////////////////////////////////////////////
    
    return application.exec();
}

// 
// main.cpp ends here

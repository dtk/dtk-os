// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.13
import QtQuick.Controls 1.4
import QtQuick.Window 2.13

Item {

// /////////////////////////////////////////////////////////////////////////////
// TODO: Turn this into our own item, aka Surface with a prefix
// /////////////////////////////////////////////////////////////////////////////

     width: Screen.width;
    height: Screen.height;

// /////////////////////////////////////////////////////////////////////////////

    Image { id: user;

        anchors.centerIn: parent;
        source: "user";
        width: parent.width/8;
        height: parent.width/8;
        smooth: true;
    }

    TextField {
        anchors.top: user.bottom;
        anchors.horizontalCenter: user.horizontalCenter;
        anchors.topMargin: 20;
        width: 400;

        onAccepted: {
            engine.done();
        }
    }
}

//
// dtkOSRuntimeWidgetsLogin.qml ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <dtkOSRuntimeExport>

class DTKOSRUNTIME_EXPORT dtkOSRuntimeWidgetsSurface : public QWidget
{
    Q_OBJECT

public:
     dtkOSRuntimeWidgetsSurface(QWidget *parent);
    ~dtkOSRuntimeWidgetsSurface(void);

signals:
    void done(void);

public slots:
    void setContents(const QString&);

#pragma mark - Mandatory internals
    
public:
    QSize sizeHint(void) const override;

protected:
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    class dtkOSRuntimeWidgetsSurfacePrivate *d;
};

// 
// dtkOSRuntimeWidgetsSurface.h ends here

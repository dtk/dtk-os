// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.13
import QtQuick.Controls 1.4
import QtQuick.Window 2.13

Item {

// /////////////////////////////////////////////////////////////////////////////
// TODO: Turn this into our own item, aka Surface with a prefix
// /////////////////////////////////////////////////////////////////////////////
    
     width: Screen.width;
    height: Screen.height;

// /////////////////////////////////////////////////////////////////////////////

    Image { id: pear;

        anchors.centerIn: parent;
        source: "pear";
        width: parent.width/8;
        height: parent.width/8;
        smooth: true;
    }

    ProgressBar {
        anchors.top: pear.bottom;
        anchors.horizontalCenter: pear.horizontalCenter;
        anchors.topMargin: 20;
        width: 400;

        NumberAnimation on value {
            from: 0;
            to: 1;
            duration: 1000;
            running: true;
        }
    }
}

//
// dtkOSRuntimeWidgetsBoot.qml ends here

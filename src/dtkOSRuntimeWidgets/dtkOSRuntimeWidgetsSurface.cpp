// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkOSRuntimeWidgetsSurface.h"

#include <QtQuick>
#include <QtQuickWidgets>

class dtkOSRuntimeWidgetsSurfacePrivate
{
public:
    QQuickWidget *contents = nullptr;
};

dtkOSRuntimeWidgetsSurface::dtkOSRuntimeWidgetsSurface(QWidget *parent) : QWidget(parent)
{
    d = new dtkOSRuntimeWidgetsSurfacePrivate;

    QSurfaceFormat format;
    format.setAlphaBufferSize(16);
    format.setSamples(16);

    d->contents = new QQuickWidget(this);
    d->contents->engine()->addImportPath("qrc:");
    d->contents->rootContext()->setContextProperty("engine", this);
    d->contents->setFormat(format);
    d->contents->setResizeMode(QQuickWidget::SizeRootObjectToView);
    d->contents->setAttribute(Qt::WA_AlwaysStackOnTop);
    d->contents->setClearColor(Qt::transparent);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(d->contents);
}

dtkOSRuntimeWidgetsSurface::~dtkOSRuntimeWidgetsSurface(void)
{
    delete d;
}

void dtkOSRuntimeWidgetsSurface::setContents(const QString& path)
{
    d->contents->setSource(QUrl(path));
}

QSize dtkOSRuntimeWidgetsSurface::sizeHint(void) const
{
    return QSize(0, 0);
}

void dtkOSRuntimeWidgetsSurface::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.fillRect(event->rect(), QColor("#222224"));
}

void dtkOSRuntimeWidgetsSurface::resizeEvent(QResizeEvent *event)
{
    d->contents->move(
        event->size().width()/2 - d->contents->size().width()/2,
        event->size().height()/2 - d->contents->size().height()/2);

    QWidget::resizeEvent(event);
}

// 
// dtkOSRuntimeWidgetsSurface.cpp ends here
